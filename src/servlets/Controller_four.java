package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import to.ClientsTO;
import dao.ClientsDAO;
import dao.ToursHasClientsDAO;

/**
 * Servlet implementation class Controller_four
 */
public class Controller_four extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(Controller_one.class
			.getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller_four() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// ��������� ���������
		response.setContentType("text/html;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("utf-8");
		log.info("setting of CharacterEncoding has done");

		// ��������\��������� ������
		HttpSession session = request.getSession();
		log.info("session has got");

		// ��������� ���������
		try {
			String tourid = session.getAttribute("tour_id").toString();
			int tour_id = Integer.parseInt(tourid);
			log.info("successful parsing");
			
			String name = session.getAttribute("name").toString();
			ArrayList<ClientsTO> list = new ClientsDAO().searchByName(name);
			ClientsTO client = new ClientsTO();
			if (list.size() > 0)
				client = list.get(0);
			int client_id = client.getIdClient();
			log.info("getting of atrubutes has completed");
			
			// ������� �������� ����� �����
			if (new ToursHasClientsDAO().addRelation(tour_id, client_id)) {
				log.info("the attempt to redirect the request to /approved.html");
				request.getRequestDispatcher("/approved.html").forward(request, response);
			} else {
				log.info("insert new order has ended in failure");
				log.info("the attempt to redirect the request to /jsp/ordering.jsp");
				request.getRequestDispatcher("/jsp/ordering.jsp").forward(request, response);
			}
		} catch (NumberFormatException e) {
			log.warn(e);
			log.info("the attempt to redirect the request to /jsp/ordering.jsp");
			request.getRequestDispatcher("/jsp/ordering.jsp").forward(request, response);
		} catch (Exception e) {
			log.warn(e);
			log.info("the attempt to redirect the request to /jsp/ordering.jsp");
			request.getRequestDispatcher("/jsp/ordering.jsp").forward(request, response);
		}
	}

}
