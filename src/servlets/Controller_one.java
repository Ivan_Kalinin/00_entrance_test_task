package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import to.ClientsTO;
import dao.ClientsDAO;
import dao.MarketDAO;

/**
 * Servlet implementation class Controller_one
 */
public class Controller_one extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(Controller_one.class.getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller_one() {
		super();
	}
	public Controller_one() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// ��������� ���������
		response.setContentType("text/html;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("utf-8");
		log.info("setting of CharacterEncoding has done");

		// �������� ������
		HttpSession session = request.getSession(true);
		session.setMaxInactiveInterval(3600 * 24);
		log.info("session has got");

		// �������� ����������
		try {
			MarketDAO.conn = MarketDAO.getConn("root", "729225");
			log.info("connection with database has done");
		} catch (SQLException e) {
			log.error(e);
		}

		// ��������� ���������
		session.setAttribute("name", "guest");
		session.setAttribute("isRegular", false);
		session.setAttribute("access", "guest");
		log.info("setting of atrubutes has completed");

		// ������������� � jsp
		log.info("the attempt to redirect the request to /jsp/main.jsp");
		request.getRequestDispatcher("/jsp/main.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// ��������� ���������
		response.setContentType("text/html;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("utf-8");
		log.info("setting of CharacterEncoding has done");

		// ��������\��������� ������
		HttpSession session = request.getSession(true);
		session.setMaxInactiveInterval(3600 * 24);
		log.info("session has got");

		// �������� ���������� � ��
		try {
			MarketDAO.conn = MarketDAO.getConn("root", "729225");
			log.info("connection has done");
		} catch (SQLException e) {
			log.error(e);
		}

		// ���������� ����������
		String access = "client";
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		if (!login.equals("") && !password.equals("")) {
			log.info("getting attributes has completed");

			// �������� ������ �������; ��������������� �������������
			if (password.equals("729225") && login.equals("root")) {
				log.info("admin success level");
				access = "admin";
				session.setAttribute("name", "big angry admin");
				session.setAttribute("access", "admin");
				log.info("setting attributes has completed");

				log.info("the attempt to redirect the request to jsp/admin.jsp");
				request.getRequestDispatcher("jsp/admin.jsp").forward(request, response);
			} else {
				try {
					// ������������� �������
					ArrayList<ClientsTO> list = new ClientsDAO().searchByLogin(login);
					ClientsTO client = new ClientsTO();
					if (list.size() > 0)
						client = list.get(0);
					if (client.getPassword().equals(password)) {
						log.info("client access level");
						session.setAttribute("name", client.getName());
						session.setAttribute("isRegular", client.getRegularClient());
						session.setAttribute("access", access);

						// ��������������� �������
						request.getRequestDispatcher("jsp/main.jsp").forward(request, response);
					} else {
						log.info("unknown client");
						log.info("the attempt to redirect the request to index.html");
						request.getRequestDispatcher("index.html").forward(request, response);
					}
				} catch (SQLException e) {
					log.warn(e);
					log.info("the attempt to redirect the request to index.html");
					request.getRequestDispatcher("index.html").forward(request, response);
				} catch (Exception e) {
					log.warn(e);
					log.info("the attempt to redirect the request to /jsp/main.jsp");
					request.getRequestDispatcher("main.jsp").forward(request, response);
				}
			}
		} else {
			log.info("some fields is epmty");
			request.getRequestDispatcher("index.html").forward(request, response);
		}
	}
}
