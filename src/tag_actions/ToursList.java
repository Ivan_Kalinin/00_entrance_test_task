package tag_actions;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import to.ToursTO;
import dao.ToursDAO;

public class ToursList extends TagSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean isRegular;

	public void setIsRegular(boolean isRegular) {
		this.isRegular = isRegular;
	}

	public int doStartTag() throws JspException {
		ArrayList<ToursTO> list = new ArrayList<ToursTO>();
		StringBuilder table = new StringBuilder();
		table.append("<table border=\"1\"><tr>"
				+ "<th>tour id</th>"
				+ "<th>tour name</th>"
				+ "<th>price</th>"
				+ "<th>burning permit</th>"
				+ "<th>possible discount</th></tr>");

		try {
			// MarketDAO.conn = MarketDAO.getConn("root", "729225");
			list = new ToursDAO().selectTours();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ListIterator<ToursTO> it = list.listIterator();
		ToursTO temp = null;
		String permit = "no";
		if (isRegular)
			while (it.hasNext()) {
				temp = it.next();
				if (temp.getBurning_permit())
					permit = "yes";
				table.append("<tr><td>" + temp.getIdTour() + "</td>" 
								+ "<td>"+ temp.getName() + "</td>"
								+ "<td>"+ temp.getPrice() + "</td>" 
								+ "<td>" + permit + "</td>"
								+ "<td>" + temp.getPossible_discount() + "</td></tr>");
			}
		else {
			while (it.hasNext()) {
				temp = it.next();
				if (temp.getBurning_permit())
					permit = "yes";
				table.append("<tr><td>" + temp.getIdTour() + "</td>" 
								+ "<td>"+ temp.getName() + "</td>"
								+ "<td>" + temp.getPrice() + "</td>" 
								+ "<td>" + permit + "</td>"
								+ "<td> 0 </td></tr>");
			}
		}
		table.append("</table>");

		try {
			JspWriter out = pageContext.getOut();
			out.write(table.toString());
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}

		return SKIP_BODY;
	}
}
