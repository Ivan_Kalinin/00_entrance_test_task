package tag_actions;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import to.ClientsTO;
import to.ToursHasClientsTO;
import dao.ClientsDAO;
import dao.ToursHasClientsDAO;

public class ToursHasClientsList extends TagSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int doStartTag() throws JspException {
		ArrayList<ToursHasClientsTO> list = new ArrayList<ToursHasClientsTO>();
		StringBuilder table = new StringBuilder();
		table.append("<table border=\"1\">" + "<tr><th>client id</th>"
				+ "<th>tour id</th></tr>");

		try {
			// MarketDAO.conn = MarketDAO.getConn("root", "729225");
			list = new ToursHasClientsDAO().selectAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ListIterator<ToursHasClientsTO> it = list.listIterator();
		ToursHasClientsTO temp = null;
		while (it.hasNext()) {
			temp = it.next();
			table.append("<tr><td>" + temp.getClients_idclient() + "</td>" 
						 	+ "<td>" + temp.getTours_idtour() + "</td></tr>");
		}
		table.append("</table>");

		try {
			JspWriter out = pageContext.getOut();
			out.write(table.toString());
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
}
