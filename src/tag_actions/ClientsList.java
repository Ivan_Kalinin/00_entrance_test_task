package tag_actions;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import to.ClientsTO;
import dao.ClientsDAO;

public class ClientsList extends TagSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int doStartTag() throws JspException {
		ArrayList<ClientsTO> list = new ArrayList<ClientsTO>();
		StringBuilder table = new StringBuilder();
		table.append("<table border=\"1\">" + "<tr><th>id</th>"
				+ "<th>name</th>" + "<th>login</th><" + "th>password</th>"
				+ "<th>status</th></tr>");

		try {
			// MarketDAO.conn = MarketDAO.getConn("root", "729225");
			list = new ClientsDAO().selectAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ListIterator<ClientsTO> it = list.listIterator();
		ClientsTO temp = null;
		String status = "non-permanent";
		while (it.hasNext()) {
			temp = it.next();
			if(temp.getRegularClient())
				status = "regular";
			else
				status = "non-permanent";
			table.append("<tr><td>" + temp.getIdClient() + "</td>" + "<td>"
					+ temp.getName() + "</td>" + "<td>" + temp.getLogin()
					+ "</td>" + "<td>" + temp.getPassword() + "</td>" + "<td>"
					+ status + "</td></tr>");
		}
		table.append("</table>");

		try {
			JspWriter out = pageContext.getOut();
			out.write(table.toString());
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
}
