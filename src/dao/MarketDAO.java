package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public abstract class MarketDAO {
	public static Connection conn = null;
	public static Statement st = null;
	public static ResultSet res = null;

	public static Connection getConn(String user, String password)
			throws SQLException {
		if (conn != null)
			return conn;
		else {
			try {
				initConn(user, password);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return conn;
		}
	}

	protected static void initConn(String user, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("org.gjt.mm.mysql.Driver");
		Properties p = new Properties();
		// root 729225
		p.setProperty("user", user);
		p.setProperty("password", password);
		p.setProperty("useUnicode", "true");
		p.setProperty("characterEncoding", "utf-8");
		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/travel_agency", p);
		st = conn.createStatement();
	}

	public static void closeConn() throws SQLException {
		try {
			if (res != null)
				res.close();
			if (st != null)
				st.close();
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
