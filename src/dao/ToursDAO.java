package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import to.ToursTO;
  // �������� �������
  // �������� �������// �������� �������
  // �������� �������
  // �������� �������

public static Connection conn = null;
	public static Statement st = null;
	public static ResultSet res = null;

public class ToursDAO extends MarketDAO implements IToursDAO {

	public ArrayList<ToursTO> selectTours(int limit) throws SQLException {
		// �������� �������
		res = st.executeQuery("SELECT * FROM tours;");

		// ���������� ��������� lim
		res.last();
		int resulSetSize = res.getRow();
		if (resulSetSize < limit)
			limit = resulSetSize;
		res.beforeFirst();

		// ������ ������
		ArrayList<ToursTO> list = new ArrayList<ToursTO>();
		int i = 1;
		while (res.next() && i < limit) {
			list.add(new ToursTO(res.getInt(1), res.getString(2), res
					.getDouble(3), res.getBoolean(4), res.getDouble(5)));
			i++;
		}

		return list;
	}

	public ArrayList<ToursTO> selectTours() throws SQLException {
		// �������� �������
		res = st.executeQuery("SELECT * FROM tours;");

		// ������ ������
		ArrayList<ToursTO> list = new ArrayList<ToursTO>();
		while (res.next()) {
			list.add(new ToursTO(res.getInt(1), res.getString(2), res
					.getDouble(3), res.getBoolean(4), res.getDouble(5)));
		}

		return list;
	}

	public ToursTO getTourById(int id) throws SQLException {
		// �������� �������
		String query = "SELECT * FROM tours WHERE tours.idtour = ?";
		PreparedStatement ps = MarketDAO.conn.prepareStatement(query);
		ps.setInt(1, id);

		// ������; ��������� ������
		res = ps.executeQuery();
		ToursTO tour = new ToursTO();
		if (res.next()) {
			tour = new ToursTO(res.getInt(1), res.getString(2),
					res.getDouble(3), res.getBoolean(4), res.getDouble(5));
		}
		return tour;

	}

	public void deleteTourById(int id) throws SQLException {
		// �������� �������
		// �� ����� ����������� ������ PreparedStatement ����� �� ��������, 
		// ����� ������ �������������
		String query = "DELETE FROM tours WHERE tours.idtour = '" + id + "'";

		// ������; ��������� ������
		st.executeUpdate(query);
	}
}
