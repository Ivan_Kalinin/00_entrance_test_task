package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import to.ToursHasClientsTO;

interface IToursHasClientsDAO {
	public ArrayList<ToursHasClientsTO> selectAll() throws SQLException;

	public boolean addRelation(int client_id, int tour_id) throws SQLException;
}

public class ToursHasClientsDAO extends MarketDAO implements
		IToursHasClientsDAO {
	public ArrayList<ToursHasClientsTO> selectAll() throws SQLException {
		// �������� �������
		res = st.executeQuery("SELECT * FROM tours_has_clients");

		// ������ ������
		ArrayList<ToursHasClientsTO> list = new ArrayList<ToursHasClientsTO>();
		while (res.next()) {
			list.add(new ToursHasClientsTO(res.getInt(1), res.getInt(2)));
		}

		return list;
	}

	public boolean addRelation(int tour_id, int client_id) throws SQLException {
		// �������� �������
		String query = "INSERT INTO tours_has_clients(tours_idtour, clients_idclient) "
				+ "VALUES (?, ?)";
		PreparedStatement ps = MarketDAO.conn.prepareStatement(query);
		ps.setInt(1, tour_id);
		ps.setInt(2, client_id);

		// ������;
		if (ps.executeUpdate() > 0)
			return true;
		else
			return false;
	}
}
