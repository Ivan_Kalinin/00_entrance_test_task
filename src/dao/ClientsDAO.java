package dao;

import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;
import java.sql.PreparedStatement;

import to.ClientsTO;

interface IClientsDAO {
	public ArrayList<ClientsTO> selectAll() throws SQLException;

	public ArrayList<ClientsTO> searchByLogin(String login) throws SQLException;

	public ArrayList<ClientsTO> searchByName(String name) throws SQLException;

	public boolean addClient(String name, String login, String password)
			throws SQLException, SQLTimeoutException;

	public void deleteClientById(int id) throws SQLException;
}

public class ClientsDAO extends MarketDAO implements IClientsDAO {

	public ArrayList<ClientsTO> selectAll() throws SQLException {
		// �������� �������
		String query = "SELECT * FROM clients";

		// ������; ��������� ������
		ArrayList<ClientsTO> list = new ArrayList<ClientsTO>();
		res = st.executeQuery(query);
		while (res.next()) {
			list.add(new ClientsTO(res.getInt(1), res.getString(2), res
					.getString(3), res.getString(4), res.getBoolean(5)));
		}

		return list;
	}

	public ArrayList<ClientsTO> searchByLogin(String login) throws SQLException {
		// �������� �������
		String query = "SELECT * FROM clients WHERE clients.login = ?";
		PreparedStatement ps = MarketDAO.conn.prepareStatement(query);
		ps.setString(1, login);

		// ������; ��������� ������
		ArrayList<ClientsTO> list = new ArrayList<ClientsTO>();
		res = ps.executeQuery();
		while (res.next()) {
			list.add(new ClientsTO(res.getInt(1), res.getString(2), res
					.getString(3), res.getString(4), res.getBoolean(5)));
		}

		return list;
	}

	public ArrayList<ClientsTO> searchByName(String name) throws SQLException {
		// �������� �������
		String query = "SELECT * FROM clients WHERE clients.name = ?";
		PreparedStatement ps = MarketDAO.conn.prepareStatement(query);
		ps.setString(1, name);

		// ������; ��������� ������
		ArrayList<ClientsTO> list = new ArrayList<ClientsTO>();
		res = ps.executeQuery();
		while (res.next()) {
			list.add(new ClientsTO(res.getInt(1), res.getString(2), res
					.getString(3), res.getString(4), res.getBoolean(5)));
		}

		return list;
	}

	public boolean addClient(String name, String login, String password)
			throws SQLException, SQLTimeoutException {
		// �������� ������������ ������
		if (searchByLogin(login).size() > 0 || searchByName(name).size() > 0)
			return false;

		// �������� �������
		String query = "INSERT INTO clients(name, login, password) "
				+ "VALUES (?, ?, ?)";
		PreparedStatement ps = MarketDAO.conn.prepareStatement(query);
		ps.setString(1, name);
		ps.setString(2, login);
		ps.setString(3, password);

		// ������;
		ps.executeUpdate();

		return true;
	}

	public void deleteClientById(int id) throws SQLException {
		// �������� �������
		// �� ����� ����������� ������ PreparedStatement ����� �� ��������,
		// ����� ������ �������������
		String query = "DELETE FROM clients WHERE clients.idclient = '" + id + "'";

		// ������; ��������� ������
		st.executeUpdate(query);

	}
}
