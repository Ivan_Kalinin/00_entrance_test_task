package to;

public class ClientsTO {
	private int idclient;
	private String name;
	private String login;
	private String password;
	private boolean regular_client;

	public ClientsTO(int idclient, String name, String login, String password,
			boolean regular_client) {
		this.idclient = idclient;
		this.name = name;
		this.login = login;
		this.password = password;
		this.regular_client = regular_client;
	}
	
	public ClientsTO() {
		this.idclient = 0;
		this.name = "";
		this.login = "";
		this.password = "";
		this.regular_client = false;
	}
	
	public int getIdClient() {
		return this.idclient;
	}

	public void setIdClient(int idclient) {
		this.idclient = idclient;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean getRegularClient() {
		return this.regular_client;
	}

	public void setRegularClient(boolean regular_client) {
		this.regular_client = regular_client;
	}
}
