package to;

public class ToursHasClientsTO {
	private int tours_idtour;
	private int clients_idclient;

	public ToursHasClientsTO(int tours_idtour, int clients_idclient) {
		this.tours_idtour = tours_idtour;
		this.clients_idclient = clients_idclient;
	}

	public ToursHasClientsTO() {
		this.tours_idtour = 0;
		this.clients_idclient = 0;
	}

	public int getTours_idtour() {
		return this.tours_idtour;
	}

	public void setTours_idtour(int tours_idtour) {
		this.tours_idtour = tours_idtour;
	}

	public int getClients_idclient() {
		return this.clients_idclient;
	}

	public void setClients_idclient(int clients_idclient) {
		this.clients_idclient = clients_idclient;
	}
}
