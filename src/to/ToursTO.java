package to;

public class ToursTO {
	private int idtour;
	private String name;
	private double price;
	private boolean burning_permit;
	private double possible_discount;

	public ToursTO(int idtour, String name, double price,
			boolean buning_permit, double possible_discount) {
		this.idtour = idtour;
		this.name = name;
		this.price = price;
		this.burning_permit = buning_permit;
		this.possible_discount = possible_discount;
	}

	public ToursTO() {
		this.idtour = 0;
		this.name = "";
		this.price = 0;
		this.burning_permit = false;
		this.possible_discount = 0;
	}

	public int getIdTour() {
		return this.idtour;
	}

	public void setIdTour(int idtour) {
		this.idtour = idtour;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean getBurning_permit() {
		return this.burning_permit;
	}

	public void setBurning_permit(boolean burning_permit) {
		this.burning_permit = burning_permit;
	}

	public double getPossible_discount() {
		return this.possible_discount;
	}

	public void setPossible_discount(double possible_discount) {
		this.possible_discount = possible_discount;
	}
}
