package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.ClientsDAO;
import dao.MarketDAO;

/**
 * Servlet implementation class Controller_two
 */
public class Controller_two extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(Controller_one.class.getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller_two() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// ��������� ���������
		response.setContentType("text/html;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("utf-8");
		log.info("setting of CharacterEncoding has done");

		// ��������\��������� ������
		HttpSession session = request.getSession(true);
		session.setMaxInactiveInterval(3600 * 24);
		log.info("session has got");

		// ���������� ����������
		String access = "client";
		String name = request.getParameter("name");
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		if(password.equals(request.getParameter("confirm_password"))) {
			log.info("parameters has filled");
	
			try {
				// �������� ���������� � ��
				MarketDAO.conn = MarketDAO.getConn("root", "729225");
				log.info("connection has done");
	
				// ������� ����������� ������ �������
				if (new ClientsDAO().addClient(name, login, password)) {
					// ������ ������ � ������
					session.setAttribute("name", name);
					session.setAttribute("isRegular", false);
					session.setAttribute("access", access);
					log.info("setting attributes has completed");
	
					// ��������������� �������
					log.info("the attempt to redirect the request to /jsp/main.jsp");
					request.getRequestDispatcher("/jsp/main.jsp").forward(request, response);
				} else {
					// ������ ��������� � ��������������� �������
					request.setAttribute("message", "enter other id data!");
					log.info("the attempt to redirect the request to registration.jsp");
					request.getRequestDispatcher("jsp/registration.jsp").forward(request, response);
				} 
			} catch (SQLException e) {
				log.info("the attempt to redirect the request to registration.jsp");
				request.getRequestDispatcher("jsp/registration.jsp").forward(request, response);
			}
		} else {
			// ������������ �������. ������� �� �������� �����������
			request.setAttribute("message", "different passwords");
			log.info("password confirmation failure");
			log.info("the attempt to redirect the request to registration.jsp");
			request.getRequestDispatcher("jsp/registration.jsp").forward(request, response);
		}
	}

}
