package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import dao.ClientsDAO;
import dao.ToursDAO;

/**
 * Servlet implementation class Controller_admin
 */
public class Controller_admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(Controller_one.class.getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller_admin() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// ��������� ���������
		response.setContentType("text/html;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("utf-8");
		log.info("setting of CharacterEncoding has done");

		// ������ ����������
		String tour_id = request.getParameter("tour_id");
		String client_id = request.getParameter("client_id");
		log.info("session has got");

		// �������� �������\����
		if (tour_id != null) {
			try {
				String id = request.getParameter("tour_id");
				int tour = Integer.parseInt(id);
				new ToursDAO().deleteTourById(tour);
				log.info("attempted to delete tour");
			} catch (SQLException e) {
				log.warn(e);
			} catch (NumberFormatException ex) {
				log.warn(ex);
			} catch (Exception e) {
				log.warn(e);
			} finally {
				request.getRequestDispatcher("/admin.jsp").forward(request, response);
			}
		} else if (client_id != null) {
			try {
				String id = request.getParameter("client_id");
				int client = Integer.parseInt(id);
				log.info("successful parsing");
				new ClientsDAO().deleteClientById(client);
				log.info("attempted to delete client");
			} catch (SQLException e) {
				log.warn(e);
			} catch (NumberFormatException ex) {
				log.warn(ex);
			} catch (Exception e) {
				log.warn(e);
			} finally {
				request.getRequestDispatcher("/admin.jsp").forward(request, response);
			}
		} else
			log.info("removal has failed");
			request.getRequestDispatcher("/admin.jsp").forward(request, response);
	}
}