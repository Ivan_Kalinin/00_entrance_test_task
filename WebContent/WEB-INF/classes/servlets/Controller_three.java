package servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import to.ClientsTO;
import to.ToursTO;
import dao.ClientsDAO;
import dao.ToursDAO;

/**
 * Servlet implementation class Controller_three
 */
public class Controller_three extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(Controller_one.class.getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller_three() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// ��������� ���������
		response.setContentType("text/html;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("utf-8");
		log.info("setting of CharacterEncoding has done");

		// ��������\��������� ������
		HttpSession session = request.getSession();
		log.info("session has got");

		try {
			String id = session.getAttribute("tour_id").toString();
			int tour_id = Integer.parseInt(id);
			log.info("successful parsing");

			// �������� ������� '���' 
			ToursTO tour = new ToursDAO().getTourById(tour_id);
			log.info("tour TO has created");
			
			// ��������� � ������ ����������  
			session.setAttribute("tour_name", tour.getName());
			String regular = session.getAttribute("isRegular").toString();
			log.info("reading and writing of attributes");

			if (regular != null)
				if (regular.equals("true")) {
					double total_price = tour.getPrice() - tour.getPossible_discount();
					session.setAttribute("total_price", total_price);
				} else
					session.setAttribute("total_price", tour.getPrice());
			log.info("total price has calculated");
			
			log.info("the attempt to redirect the request to /jsp/ordering.jsp");
			request.getRequestDispatcher("/jsp/ordering.jsp").forward(request, response);
		} catch (SQLException e) {
			log.warn(e);
			log.info("the attempt to redirect the request to /jsp/main.jsp");
			request.getRequestDispatcher("jsp/main.jsp").forward(request, response);
		} catch (NumberFormatException e) {
			log.warn(e);
			log.info("the attempt to redirect the request to /jsp/main.jsp");
			request.getRequestDispatcher("jsp/main.jsp").forward(request, response);
		} catch (Exception e) {
			log.warn(e);
			log.info("the attempt to redirect the request to /jsp/main.jsp");
			request.getRequestDispatcher("jsp/main.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// ��������� ���������
		response.setContentType("text/html;charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("utf-8");
		log.info("request.setCharacterEncoding");

		// ��������\��������� ������
		HttpSession session = request.getSession();
		log.info("request.getSession");

		try {
			// ��������� ������ ����
			String id = (String) request.getParameter("tour_id");
			int tour_id = Integer.parseInt(id);
			log.info("successful parsing");

			// �� ������� �������� ���� �� ���������� ������ �������.
			// File file = new File("tour_info/" + filename + ".txt");
			
			// ������� � ���� �� ������� �� ������� ������������ ������� �����
			File file = new File("C:/test/tour_info/" + tour_id + ".txt");

			// ������� �������� ������ �� �����
			log.info("the attempt to read data from file");
			BufferedReader reader = null;
			if (file.exists()) {
				reader = new BufferedReader(new FileReader(file));
				String line, text = "";
				while ((line = reader.readLine()) != null)
					text += line + "<br/>";
				log.info("reading is successful");
				
				// ������ id � ����������� ����� � ��������
				session.setAttribute("tour_id", tour_id);
				request.setAttribute("info", text);
				log.info("setting of atrubutes has completed");
			} else 
				log.warn("file not exist");
		} catch (NumberFormatException ex) {
			log.warn(ex);
		} catch (Exception e) {
			log.warn(e);
		} finally {
			// ��������������� �������
			log.info("the attempt to redirect the request to /jsp/main.jsp");
			request.getRequestDispatcher("jsp/main.jsp").forward(request, response);
		}
	}
}
