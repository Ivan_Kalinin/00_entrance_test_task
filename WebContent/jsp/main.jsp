<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/lib/tourslist.tld" prefix="tourslist"%>

<html>
<head>
	<title>MANGOU</title>
</head>

<body>
	<div align="center">
		
		<font color="#E4926E"> <c:out value="hello, ${name}!" /> <br />
			<c:out value="please, smile and select your dream!" /> <br /> 
			<c:if test="${isRegular == 'true'}">
				<c:out value="(you are regular client! use discounts!)" />
			</c:if>
		</font> <br />
		<tourslist:getinfo isRegular="${isRegular}" />

		<br />
		<form action="Controller_three" method="post">
			<input type="submit" value="more info" /> 
			<input type="text" value="tour_id" name="tour_id" required="required"
				onblur="appear(this)" onfocus="disappear(this)" maxlength="3"
				pattern="[0-9]{,3}" oninvalid="alert('USE ONLY NUMBERS!');" /> 
		</form>


		<c:if test="${not empty access and access == 'guest'}">
			<form action="index.html">
				<p>
					<font color="#76785C"> 
						<c:out value="(guests can not order)" />
					</font> 
					<input type="submit" value="sign in" />
				</p>
			</form>
		</c:if>

		<c:if test="${not empty info}">
			${info} <br />
			<c:if test="${not empty access and access == 'client'}">
				<form action="Controller_three" method="get">
					<input type="submit" name="buy" value="order it now!" />
				</form>
			</c:if>
		</c:if>
	</div>

	<script src="clearing_forms.js"></script>
</body>

</html>