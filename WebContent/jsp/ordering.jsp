<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
	<title>MANGOU</title>
</head>

<body>
	<div align="center">
	
		<font color="#E4926E"> 
			<c:out value="you have selected '${tour_name}' tour" /> <br />
			<c:out value="total price: ${total_price} " /> <br />
			<c:out value="please, fill the gaps" /> <br />
		</font>
	
		<br /> 
		<form action="Controller_four" method="post">
			<input type="text" value="first name" name="first name" required="required" 
				onblur="appear(this)" onfocus="disappear(this)" maxlength="16"
				oninvalid="alert('USE ONLY LETTERS AND NUMBERS!');" pattern="^[a-zA-Z]+$"/>
			<Br /> 
			<input type="text" value="last name" name="last name" required="required" 
				onblur="appear(this)" onfocus="disappear(this)" maxlength="16"
				oninvalid="alert('USE ONLY LETTERS AND NUMBERS!');" pattern="^[a-zA-Z]+$"/>
			<Br /> 
			<input type="text" value="card number" name="card number" required="required" 
				onblur="appear(this)" onfocus="disappear(this)" maxlength="16"
				oninvalid="alert('USE ONLY LETTERS AND NUMBERS!');" pattern="[0-9]{16}"/>
			<Br /> 
			<input type="text" value="CVV" name="CVV" required="required" 
				onblur="appear(this)" onfocus="disappear(this)" maxlength="3"
				oninvalid="alert('USE ONLY LETTERS AND NUMBERS!');" pattern="[0-9]{3}"/> 
			<Br /> 
			<input type="text" value="expiration date" name="expiration date" required="required" 
				onblur="appear(this)" onfocus="disappear(this)" maxlength="5"
				pattern="[0-9]{2}/[0-9]{2}"/>  
			<Br /> <Br /> 
			<input type="submit" value="pay for this tour" />
		</form>
	</div>
	
	<script src="clearing_forms.js"></script>
</body>

</html>