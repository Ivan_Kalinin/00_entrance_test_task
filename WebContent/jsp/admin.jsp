<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/lib/clientslist.tld" prefix="clientslist"%>
<%@ taglib uri="/WEB-INF/lib/tourslist.tld" prefix="tourslist"%>
<%@ taglib uri="/WEB-INF/lib/tourshasclientslist.tld" prefix="tourshasclientslist"%>

<html>
<head>
	<title>MANGOU</title>
</head>

<body>
	<div align="center">
		<tourshasclientslist:getinfo />
		<br />
		<br />
		<tourslist:getinfo isRegular="true"/> <br />
		<form action="Controller_admin" method="post">
			<input type="text" value="tour_id" name="tour_id" required="required" 
				onblur="appear(this)" onfocus="disappear(this)" maxlength="3"
				pattern="[0-9]{,3}" oninvalid="alert('USE ONLY NUMBERS!');" /> 
			<br />
			<br />
			<input type="submit" value="delete tour" />
		</form>
		
		<br />
		<clientslist:getinfo />
		<form action="Controller_admin" method="post">
			<input type="text" value="client_id" name="client_id" required="required" 
				onblur="appear(this)" onfocus="disappear(this)" maxlength="20"/> 
			<br /> 
			<br />
			<input type="submit" value="delete client" />
		</form>
		
	</div>
	
	<script src="js/clearing_forms.js"></script>
</body>

</html>