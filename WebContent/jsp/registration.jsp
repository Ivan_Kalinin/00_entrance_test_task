<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
	<title>MANGOU</title>

</head>

<body>
	<div align="center">
		<font color="#E6865D"> 
			<b> <c:out value="TRAVEL AGENCY \"MANGOU\"" /> </b> <br />
			<c:out value="registration" />
		</font>
	
		<br /> <br />
	
		<c:choose>
			<c:when test="${empty message}">
				<font color="#5AAE6D"> enter your id data below: </font>
			</c:when>
			<c:otherwise >
				<font color="#AE5A76"> ${message} </font>
			</c:otherwise>
		</c:choose>
	
		<form action="../Controller_two" method="post">
			<input type="text" name="name" oninvalid="alert('USE ONLY LETTERS AND NUMBERS!');" 
					pattern="[A-Za-z]+" value="name" required="required" 
					onblur="appear(this)" onfocus="disappear(this)" /> 
			<Br /> 
			<input type="text" name="login" oninvalid="alert('USE ONLY LETTERS AND NUMBERS!');" 
					pattern="\w+" value="login" required="required" 
					onblur="appear(this)" onfocus="disappear(this)" /> 
			<Br /> 
			<input type="text" name="password" oninvalid="alert('USE ONLY LETTERS AND NUMBERS!');" 
					pattern="\w+" value="password" required="required" 
					onblur="appear(this)" onfocus="disappear(this)" /> 
			<Br /> 
			<input type="text" name="confirm_password" oninvalid="alert('USE ONLY LETTERS AND NUMBERS!');" 
					pattern="\w+" value="confirm_password" required="required" 
					onblur="appear(this)" onfocus="disappear(this)" /> 
			<Br /> <Br />
			<input type="submit" value="sign up" />
		</form>
		<form action="../Controller_one" method="get">
			<p> <input type="submit" value="stay guest" /> </p>
		</form>
	</div>

	<!-- не понимаю, как занести путь в web.xml, если такое возможно.
		 здесь пользуюсь относительным путем. потому после нектороых 
		 переходов скрипт не виден -->
	<script src="../clearing_forms.js"></script>
</body>
</html>